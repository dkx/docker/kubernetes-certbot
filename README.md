# DKX/Docker/KubernetesCertbot

Uses [certbot](https://github.com/certbot/certbot) to generate [Let's encrypt certificates](https://letsencrypt.org/)
and stores them as [kubernetes secrets](https://kubernetes.io/docs/concepts/configuration/secret/).

Based on [dwnld/kubernetes-certbot](https://github.com/dwnld/kubernetes-certbot/tree/master).

## Usage

Create [deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/) for certbot:

**Instead of using the `emptyDir` storage, you should use some real
[storage](https://kubernetes.io/docs/concepts/storage/) on your cluster.**

```yaml
kind: Deployment
apiVersion: apps/v1

metadata:
  namespace: default
  name: certbot

spec:
  replicas: 1
  selector:
    matchLabels:
      name: certbot
  template:
    metadata:
      name: certbot
      labels:
        name: certbot
    spec:
      volumes:
      - name: certificates
        emptyDir: {}
      containers:
      - image: registry.gitlab.com/dkx/docker/kubernetes-certbot
        name: certbot
        imagePullPolicy: Always
        ports:
        - name: certbot
          containerPort: 80
        volumeMounts:
        - mountPath: /etc/letsencrypt
          name: certificates
        env:
        - name: DOMAINS
          value: example.com,www.example.com
        - name: EMAIL
          value: info@example.com
        - name: SECRET_NAME
          value: example-certificate
        - name: SECRET_NAMESPACE
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace
        livenessProbe:
          exec:
            command: ["exit", "0"]
        readinessProbe:
          exec:
            command: ["exit", "0"]
```

## Staging ACME server

By default the `https://acme-v02.api.letsencrypt.org/directory` ACME server is used. This can be changed by providing 
`ACME_SERVER` environment variable.

```yaml
# ...
spec:
  # ...
  template:
    # ...
    spec:
      # ...
      containers:
      - image: registry.gitlab.com/dkx/docker/kubernetes-certbot
        # ...
        env:
        - name: ACME_SERVER
          value: https://acme-staging.api.letsencrypt.org/directory
        # ...
```
