FROM alpine:3.8
LABEL maintainer="David Kudera <kudera.d@gmail.com>"

ENV KUBECTL_VERSION="v1.12.2"

COPY generate-cert.sh /generate-cert.sh
COPY run.sh /run.sh

RUN apk add --no-cache --update curl bash certbot ca-certificates && \
	curl -LO https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl && \
	chmod +x kubectl /generate-cert.sh /run.sh && \
	mv kubectl /usr/local/bin/kubectl

EXPOSE 80
VOLUME /etc/letsencrypt
CMD ["/run.sh"]
