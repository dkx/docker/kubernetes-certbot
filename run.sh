#!/usr/bin/env sh

set -eu

while true; do
    # Sleep 5 minutes so a crash looping pod doesn't punch let's encrypt.
    echo "Renewing cert in 5 minutes"
    sleep 300
    /generate-cert.sh
    echo "Certs renewed; see you tomorrow"
    sleep 86100
done
