#!/usr/bin/env sh

set -eu

ACME_SERVER=${ACME_SERVER:-https://acme-v02.api.letsencrypt.org/directory}

if [[ -z "${DOMAIN_MAIN}" ]]; then
	DOMAIN_MAIN=$(echo ${DOMAINS} | sed 's/,.*//')
fi

echo "Generating certificate ${DOMAIN_MAIN}, using ACME server ${ACME_SERVER}"
certbot \
	--non-interactive \
	--agree-tos \
	--standalone \
	--expand \
	--preferred-challenges http \
	--cert-name "${DOMAIN_MAIN}" \
	--server "${ACME_SERVER}" \
	--email "${EMAIL}" \
	--domains "${DOMAINS}" \
	certonly

echo "Saving kubernetes secret ${SECRET_NAME} in namespace ${SECRET_NAMESPACE}"
kubectl apply -f - <<EOF
kind: Secret
apiVersion: v1
metadata:
  name: "${SECRET_NAME}"
  namespace: "${SECRET_NAMESPACE}"
type: Opaque
data:
  tls.crt: "$(cat /etc/letsencrypt/live/${DOMAIN_MAIN}/fullchain.pem | base64 | tr -d '\n')"
  tls.key: "$(cat /etc/letsencrypt/live/${DOMAIN_MAIN}/privkey.pem | base64 | tr -d '\n')"
EOF
